FROM python:2-alpine

WORKDIR /opt/app

COPY requirements.txt .

RUN apk add --no-cache \
        build-base \
        python-dev \
        mariadb-dev \
        jpeg-dev \
        zlib-dev \
        nodejs \
        npm \
        git \
    && sed '/st_mysql_options options;/a unsigned int reconnect;' /usr/include/mysql/mysql.h -i.bkp \
    && pip install --no-cache-dir -r requirements.txt \
    && npm install --no-cache -g bower

COPY . .

RUN bower install --allow-root \
    && bower cache clean --allow-root \
    && python manage.py collectstatic --noinput

EXPOSE 8000

ENTRYPOINT ["./entrypoint.sh"]