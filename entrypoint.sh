#!/bin/sh

python manage.py migrate

gunicorn aquanine.wsgi:application --workers 2 --bind :8000
