# encoding=utf8
import hashlib

from django.core.validators import RegexValidator, MinLengthValidator
from django.utils.encoding import python_2_unicode_compatible

import os

from django.db import models
from datetime import datetime, date, timedelta
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from froala_editor.fields import FroalaField

from django.utils.text import slugify

TODAY = date.today()
YESTERDAY = (TODAY - timedelta(1))
LAST2DAY = (TODAY - timedelta(2))

month_lists = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน',
               'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']

size_choice = (('s', 'S'), ('m', 'M'), ('l', 'L'), ('xl', 'XL'), ('xxl', 'XXL'))
colours_choice = (
    ('White', 'ขาว'), ('Black', 'ดำ'), ('Red', 'แดง'), ('Blue', 'น้ำเงิน'), ('Navy Blue', 'กรม'),
    ('Top Black', 'ท้อปดำ'),
    ('Top Gray', 'ท้อปเทา'), ('Black/Black', 'ดำ/ดำ'), ('Black/Red', 'ดำ/แดง'))
provinces_choice = (
    (u"กรุงเทพมหานคร", "กรุงเทพมหานคร"), (u"กระบี่", "กระบี่"), (u"กาญจนบุรี", "กาญจนบุรี"),
    (u"กาฬสินธุ์", "กาฬสินธุ์"),
    (u"กำแพงเพชร", "กำแพงเพชร"), (u"ขอนแก่น", "ขอนแก่น"), (u"จันทบุรี", "ขอนแก่น"), (u"ฉะเชิงเทรา", "ฉะเชิงเทรา"),
    (u"ชลบุรี", "ชลบุรี"), (u"ชัยนาท", "ชัยนาท"), (u"ชัยภูมิ", "ชัยภูมิ"), (u"ชุมพร", "ชุมพร"),
    (u"เชียงราย", "เชียงราย"),
    (u"เชียงใหม่", "เชียงใหม่"), (u"ตรัง", "ตรัง"), (u"ตราด", "ตราด"), (u"ตาก", "ตาก"), (u"นครนายก", "นครนายก"),
    (u"นครปฐม", "นครปฐม"), (u"นครพนม", "นครพนม"), (u"นครราชสีมา", "นครราชสีมา"), (u"นครศรีธรรมราช", "นครศรีธรรมราช"),
    (u"นครสวรรค์", "นครสวรรค์"), (u"นนทบุรี", "นนทบุรี"), (u"นราธิวาส", "นราธิวาส"), (u"น่าน", "น่าน"),
    (u"บุรีรัมย์", "บุรีรัมย์"), (u"บึงกาฬ", "บึงกาฬ"), (u"ปทุมธานี", "ปทุมธานี"),
    (u"ประจวบคีรีขันธ์", "ประจวบคีรีขันธ์"),
    (u"ปราจีนบุรี", "ปราจีนบุรี"), (u"ปัตตานี", "ปัตตานี"), (u"พระนครศรีอยุธยา", "พระนครศรีอยุธยา"),
    (u"พะเยา", "พะเยา"),
    (u"พังงา", "พังงา"), (u"พัทลุง", "พัทลุง"), (u"พิจิตร", "พิจิตร"), (u"พิษณุโลก", "พิษณุโลก"),
    (u"เพชรบุรี", "เพชรบุรี"),
    (u"เพชรบูรณ์", "เพชรบูรณ์"), (u"แพร่", "แพร่"), (u"ภูเก็ต", "ภูเก็ต"), (u"มหาสารคาม", "มหาสารคาม"),
    (u"มุกดาหาร", "มุกดาหาร"), (u"แม่ฮ่องสอน", "แม่ฮ่องสอน"), (u"ยโสธร", "ยโสธร"), (u"ยะลา", "ยะลา"),
    (u"ร้อยเอ็ด", "ร้อยเอ็ด"), (u"ระนอง", "ระนอง"), (u"ระยอง", "ระยอง"), (u"ราชบุรี", "ราชบุรี"), (u"ลพบุรี", "ลพบุรี"),
    (u"ลำปาง", "ลำปาง"), (u"ลำพูน", "ลำพูน"), (u"เลย", "เลย"), (u"ศรีสะเกษ", "ศรีสะเกษ"), (u"สกลนคร", "สกลนคร"),
    (u"สงขลา", "สงขลา"), (u"สตูล", "สตูล"), (u"สมุทรปราการ", "สมุทรปราการ"), (u"สมุทรสงคราม", "สมุทรสงคราม"),
    (u"สมุทรสาคร", "สมุทรสาคร"), (u"สระแก้ว", "สระแก้ว"), (u"สระบุรี", "สระบุรี"), (u"สิงห์บุรี", "สิงห์บุรี"),
    (u"สุโขทัย", "สุโขทัย"), (u"สุพรรณบุรี", "สุพรรณบุรี"), (u"สุราษฎร์ธานี", "สุราษฎร์ธานี"),
    (u"สุรินทร์", "สุรินทร์"),
    (u"หนองคาย", "หนองคาย"), (u"หนองบัวลำภู", "หนองบัวลำภู"), (u"อ่างทอง", "อ่างทอง"), (u"อำนาจเจริญ", "อำนาจเจริญ"),
    (u"อุดรธานี", "อุดรธานี"), (u"อุตรดิตถ์", "อุตรดิตถ์"), (u"อุทัยธานี", "อุทัยธานี"),
    (u"อุบลราชธานี", "อุบลราชธานี"))

pays_choice = (('bank', 'โอนเงินเข้าบัญชีธนาคาร (ที่สาขา, ตู้ ATM หรือ ออนไลน์)'),)
deliveries_choice = (('normal', 'ธรรมดาแบบลงทะเบียน 2-10 วัน (30 บาท)'), ('ems', 'EMS ด่วนพิเศษ 1-2 วัน (50 บาท)'))
bank_choice = (('', 'กรุณาเลือกธนาคาร'), ('scb', 'ธนาคารไทยพาณิชย์'), ('kbank', 'ธนาคารกสิกรไทย'))
last2day_choice = (('', 'กรุณาเลือกวันที่'), (TODAY, '{0} {1}'.format(TODAY.day, month_lists[TODAY.month - 1])),
                   (YESTERDAY, '{0} {1}'.format(YESTERDAY.day, month_lists[YESTERDAY.month - 1])),
                   (LAST2DAY, '{0} {1}'.format(LAST2DAY.day, month_lists[LAST2DAY.month - 1])))


def validate_image_extension(value):
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.png', '.jpg', '.jpeg']
    if not ext.lower() in valid_extensions:
        raise ValidationError('File is not Allowed!')
    return value


def create_hash(url):
    hash = hashlib.sha1()
    hash.update(url)
    return hash.hexdigest()[:-10]


def new_filename(filename):
    extension = filename.split('.')[-1]
    filename_hash = create_hash(filename)
    subdir1 = filename_hash[-3:]
    subdir2 = filename_hash[-6:-3]
    new_filename = '{0}.{1}'.format(filename_hash, extension)
    fullpath_file = os.path.join(subdir1, subdir2, new_filename)
    return fullpath_file


def upload_file(instance, filename):
    full_path = new_filename(filename)
    return full_path


def upload_to_file(instance, filename):
    full_path = new_filename(filename)
    return u'announces/{0}'.format(full_path)


def slip_upload(instance, filename):
    full_path = new_filename(filename)
    return u'slips/{0}'.format(full_path)


def product_upload(instance, filename):
    full_path = new_filename(filename)
    return u'products/{0}'.format(full_path)


def product_upload_to_gallery(instance, filename):
    full_path = new_filename(filename)
    return u'products/{0}'.format(full_path)


@python_2_unicode_compatible
class Member(models.Model):
    user = models.OneToOneField(User)
    phone = models.CharField(max_length=20, blank=True)
    market = models.CharField(max_length=255, blank=True)
    address = models.TextField(blank=True)
    province = models.CharField(max_length=100, choices=provinces_choice, blank=True, null=True)
    zipcode = models.CharField(max_length=5, blank=True,
                               validators=[RegexValidator(r'^\d{1,5}$'), MinLengthValidator(4)])

    def __str__(self):
        return self.user.get_full_name()

    def get_full_address(self):
        return u'{0} {1} {2} {3}'.format(self.market, self.address, self.province, self.zipcode)


@python_2_unicode_compatible
class Announce(models.Model):
    name = models.CharField(max_length=255)
    start = models.DateField(default=date.today)
    end = models.DateField(blank=True, null=True)
    image = models.FileField(upload_to=upload_to_file, validators=[validate_image_extension])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Brand(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Colour(models.Model):
    name_th = models.CharField(max_length=80, blank=True)
    name_en = models.CharField(max_length=80, blank=True)

    def __str__(self):
        return u'{0} ({1})'.format(self.name_th, self.name_en)


@python_2_unicode_compatible
class Product(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, unique=True)
    brand = models.ForeignKey(Brand, blank=True, null=True)
    category = models.ForeignKey(Category, blank=True, null=True)
    color = models.ForeignKey(Colour, blank=True, null=True)
    price = models.DecimalField(default=0, max_digits=6, decimal_places=2)
    discount = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    detail = FroalaField(blank=True, options={'heightMin': 250})
    slug = models.SlugField(allow_unicode=True)
    image = models.FileField(upload_to=product_upload, validators=[validate_image_extension])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'{} - {}'.format(self.name, self.code)

    def discount_percent(self):
        return u'{}%'.format(int((1 - (self.discount / self.price)) * 100))


@python_2_unicode_compatible
class Gallery(models.Model):
    image = models.FileField(upload_to=product_upload_to_gallery, validators=[validate_image_extension])
    product = models.ForeignKey(Product)

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class Stock(models.Model):
    name = models.CharField(max_length=255, blank=True)
    size = models.CharField(max_length=255, choices=size_choice)
    total = models.IntegerField(default=0)
    product = models.ForeignKey(Product)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'{} ({})'.format(self.size, self.total)


@python_2_unicode_compatible
class CartItem(models.Model):
    cart = models.ForeignKey("Cart")
    item = models.ForeignKey(Stock)
    quantity = models.PositiveIntegerField(default=1)
    comment = models.CharField(max_length=255, default='-')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'{} - {}'.format(self.item.product.name, self.item.product.code)


@python_2_unicode_compatible
class Cart(models.Model):
    member = models.ForeignKey(Member, blank=True, null=True)
    items = models.ManyToManyField(Stock, through=CartItem)
    total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    product_total = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    tax_delivery = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class Order(models.Model):
    member = models.ForeignKey(Member)
    cart = models.OneToOneField(Cart)
    pay_by = models.CharField(max_length=255, choices=pays_choice, blank=True, null=True)
    deliver_by = models.CharField(max_length=255, choices=deliveries_choice, blank=True, null=True)
    is_confirm = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class Payment(models.Model):
    order = models.ForeignKey(Order)
    bank = models.CharField(max_length=255, choices=bank_choice)
    on_transfer = models.DateField()
    on_time = models.CharField(max_length=100)
    money = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    slip_transfer = models.FileField(blank=True, null=True, upload_to=slip_upload,
                                     validators=[validate_image_extension])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class Article(models.Model):
    title = models.ForeignKey(Order)
    main_image = models.FileField(blank=True, null=True, upload_to=slip_upload, validators=[validate_image_extension])
    on_transfer = models.DateField()
    on_time = models.CharField(max_length=100)
    money = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    slip_transfer = models.FileField(blank=True, null=True, upload_to=slip_upload,
                                     validators=[validate_image_extension])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


@python_2_unicode_compatible
class Article(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(allow_unicode=True)
    detail = FroalaField(blank=True, options={'heightMin': 250})
    keyword_tags = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    main_image = models.FileField(blank=True, null=True, upload_to=upload_file,
                                  validators=[validate_image_extension])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(self.title, True)

        super(Article, self).save(*args, **kwargs)

    def keyword_as_list(self):
        return self.keyword_tags.split(',')
