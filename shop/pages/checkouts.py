# encoding=utf8
import random
import string

from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views import generic
from shop.models import *
from shop.forms import AddressForm

# Email Section
from django.template.loader import get_template


def random_generator(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))


def email_to_customer(order, context):
    to = [order.member.user.email]
    # to = ['soulister.o@gmail.com']
    subject = u'[Order: {0:06d}] ขอบคุณที่สั่งซื้อสินค้ากับ AquanineTH.com - Thank you for your order'.format(
        order.id)
    message = get_template('emails/order_customer.html').render(context)
    send_mail(subject, '', from_email=settings.EMAIL_SENDER, recipient_list=to, html_message=message)


def email_to_customer_register(email, context):
    to = [email]
    # to = ['soulister.o@gmail.com']
    subject = u'ยินดีต้อนรับสู่ AquanineTH.com - Welcome to AquanineTH.com'
    message = get_template('emails/registration.html').render(context)
    send_mail(subject, '', from_email=settings.EMAIL_SENDER, recipient_list=to, html_message=message)


def email_to_admin(id, context):
    to = ['aquanineth@gmail.com']
    # to = ['soulister.o@gmail.com']
    subject = u'มีการสั่งซื้อใหม่ [Order: {0:06d}]'.format(id)
    message = get_template('emails/order_admin.html').render(context)
    send_mail(subject, '', from_email=settings.EMAIL_SENDER, recipient_list=to, html_message=message)


def edit_adress(request):
    request.session.set_expiry(0)  # 5 minutes
    form = AddressForm(request.POST)
    if form.is_valid():
        request.session["first_name"] = form.cleaned_data['first_name']
        request.session["last_name"] = form.cleaned_data['last_name']
        request.session["email"] = form.cleaned_data['email']
        request.session["address"] = form.cleaned_data['address']
        request.session["province"] = form.cleaned_data['province']
        request.session["zipcode"] = form.cleaned_data['zipcode']
        request.session["phone"] = form.cleaned_data['phone']

        messages.add_message(request, messages.SUCCESS, 'แก้ไขที่อยู่จัดส่งแล้ว')
    else:
        messages.add_message(request, messages.SUCCESS, 'ไม่สารถมารถแก้ไขที่อยู่จัดส่งได้กรุณาลองอีกครั้ง!')

    return HttpResponseRedirect(reverse('shop:checkout_confirm'))


def edit_delivery(request):
    request.session.set_expiry(0)  # 5 minutes
    cart_id = request.session.get("cart_id")
    deliver = request.POST['deliver_by']
    deliver_price = 30
    if deliver == 'ems':
        deliver_price = 50

    cart = Cart.objects.get(pk=cart_id)
    cart.tax_delivery = deliver_price
    cart.total = cart.product_total + deliver_price
    cart.save()

    request.session['deliver_by'] = deliver
    messages.add_message(request, messages.SUCCESS, 'แก้ไขวิธีการจัดส่งแล้ว')

    return HttpResponseRedirect(reverse('shop:checkout_confirm'))


class CheckoutView(generic.View):
    def get(self, request):
        context = {}
        request.session.set_expiry(0)  # 5 minutes
        if request.user.is_authenticated():
            request.session["first_name"] = request.user.first_name
            request.session["last_name"] = request.user.last_name
            request.session["email"] = request.user.email
            request.session["address"] = request.user.member.address
            request.session["province"] = request.user.member.province
            request.session["zipcode"] = request.user.member.zipcode
            request.session["phone"] = request.user.member.phone

            return HttpResponseRedirect(reverse('shop:checkout_confirm'))

        context['form'] = AddressForm(
            initial={'first_name': request.session.get("first_name"),
                     'last_name': request.session.get("last_name"),
                     'email': request.session.get("email"),
                     'address': request.session.get("address"),
                     'province': request.session.get("province"),
                     'zipcode': request.session.get("zipcode"),
                     'phone': request.session.get("phone")})
        return render(request, 'shop/checkout.html', context)

    def post(self, request):
        context = {}
        context['form'] = form = AddressForm(request.POST)
        if form.is_valid():
            request.session["first_name"] = form.cleaned_data['first_name']
            request.session["last_name"] = form.cleaned_data['last_name']
            request.session["email"] = form.cleaned_data['email']
            request.session["address"] = form.cleaned_data['address']
            request.session["province"] = form.cleaned_data['province']
            request.session["zipcode"] = form.cleaned_data['zipcode']
            request.session["phone"] = form.cleaned_data['phone']

            return HttpResponseRedirect(reverse('shop:checkout_confirm'))
        else:
            return render(request, 'shop/checkout.html', context)


class ConfirmCheckoutView(generic.View):
    def get(self, request):
        context = {}
        request.session.set_expiry(0)  # 5 minutes
        cart_id = request.session.get("cart_id")
        if cart_id:
            context['items'] = items = CartItem.objects.filter(cart=cart_id)
            context['cart'] = Cart.objects.get(pk=cart_id)
            if items.count() == 0:
                messages.add_message(request, messages.ERROR, "กรุณาเลือกซื้อสินค้าก่อน!")
                return HttpResponseRedirect(reverse('shop:index'))

            context['form'] = AddressForm(
                initial={'first_name': request.session.get("first_name"),
                         'last_name': request.session.get("last_name"),
                         'email': request.session.get("email"),
                         'address': request.session.get("address"),
                         'province': request.session.get("province"),
                         'zipcode': request.session.get("zipcode"),
                         'phone': request.session.get("phone")})

            return render(request, 'shop/confirm_order.html', context)

        messages.add_message(request, messages.ERROR, "กรุณาเลือกซื้อสินค้าก่อน!")
        return HttpResponseRedirect(reverse('shop:index'))

    def post(self, request):
        context = {}
        request.session.set_expiry(0)  # 5 minutes
        cart_id = request.session.get("cart_id")
        first_name = request.session.get("first_name")
        last_name = request.session.get("last_name")
        email = request.session.get("email")
        address = request.session.get("address")
        province = request.session.get("province")
        zipcode = request.session.get("zipcode")
        phone = request.session.get("phone")
        deliver_by = request.session.get("deliver_by")
        pay_by = 'bank'
        password = random_generator()

        user, user_created = User.objects.get_or_create(
            email=email,
            defaults={'username': email, 'password': password, 'email': email, 'first_name': first_name,
                      'last_name': last_name, 'is_active': True})

        member, member_created = Member.objects.get_or_create(
            user=user,
            defaults={'user': user, 'phone': phone, 'address': address, 'province': province, 'zipcode': zipcode})

        cart = Cart.objects.get(pk=cart_id)
        order, order_created = Order.objects.update_or_create(
            cart=cart,
            defaults={'cart': cart, 'member': member, 'pay_by': pay_by, 'deliver_by': deliver_by})

        request.session["order_id"] = order.id

        # Send email section
        context['password'] = password
        context['user'] = user
        context['order'] = order
        context['items'] = CartItem.objects.filter(cart=order.cart.id)

        if member_created:
            email_to_customer_register(email, context)

        email_to_customer(order, context)
        email_to_admin(order.id, context)

        del request.session['cart_id']

        return HttpResponseRedirect(reverse('shop:order'))
