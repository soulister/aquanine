# encoding=utf8
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.views import generic

from shop.forms import BankPaymentForm
from shop.models import *


def email_payment_to_admin(id, context):
    to = ['aquanineth@gmail.com']
    # to = ['soulister.o@gmail.com']
    subject = u'แจ้งการชำระเงิน [Order: {0:06d}]'.format(id)
    message = get_template('emails/order_confirm.html').render(context)
    send_mail(subject, '', from_email=settings.EMAIL_SENDER, recipient_list=to, html_message=message)


def confirm_payment(request, pk):
    order = Order.objects.get(pk=pk)
    order.is_confirm = True
    order.save()

    return HttpResponseRedirect(reverse("shop:order_detail", kwargs={'pk': pk}))


def order_detail(request, pk):
    context = {}
    context['order'] = order = Order.objects.get(pk=pk)
    context['items'] = CartItem.objects.filter(cart=order.cart.id)

    return render(request, 'emails/order_admin.html', context)


class OrderView(generic.View):
    def get(self, request):
        context = {}
        request.session.set_expiry(0)  # 5 minutes
        order_id = request.session.get('order_id')
        if order_id is None:
            messages.add_message(request, messages.ERROR, 'กรุณาเลือกซื้อสินค้าก่อนสั่งซื้อ')
            return HttpResponseRedirect(reverse('shop:index'))

        context['order'] = order = Order.objects.get(pk=order_id)
        context['items'] = CartItem.objects.filter(cart=order.cart.id)

        del request.session['order_id']
        del request.session['deliver_by']

        return render(request, 'shop/order.html', context)


class PaymentView(generic.View):
    def get(self, request):
        context = {}
        context['form'] = BankPaymentForm()
        return render(request, 'shop/bank_payment.html', context)

    def post(self, request):
        context = {}
        context['form'] = form = BankPaymentForm(request.POST, request.FILES)
        if form.is_valid():
            order_id = form.cleaned_data['order']
            bank_name = form.cleaned_data['bank']
            date_transfer = form.cleaned_data['on_transfer']
            time_transfer = form.cleaned_data['on_time']
            money = form.cleaned_data['money']
            slip_file = request.FILES['slip_transfer'] if 'slip_transfer' in request.FILES else ''

            order = Order.objects.get(pk=order_id)
            payment = Payment.objects.create(order=order, bank=bank_name, on_transfer=date_transfer, on_time=time_transfer, money=money, slip_transfer=slip_file)

            # Send email section
            context['order'] = order
            context['payment'] = payment
            context['items'] = CartItem.objects.filter(cart=order.cart.id)

            email_payment_to_admin(order.id, context)

            message = u'ขอบคุณสำหรับการชำระเงินค่ะ'
            messages.add_message(request, messages.SUCCESS, message)

            return HttpResponseRedirect(reverse("shop:payment"))

        message = u'กรุณากรอกข้อมูลให้ครบทุกช่องค่ะ'
        messages.add_message(request, messages.ERROR, message)
        return render(request, 'shop/bank_payment.html', context)