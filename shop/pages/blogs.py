# encoding=utf8
from django.views import generic

from shop.models import Article


class BlogListView(generic.ListView):
    ordering = ['-id']
    model = Article
    template_name = "shop/blog_list.html"
    paginate_by = 10


class BlogDetailView(generic.DetailView):
    model = Article
    template_name = "shop/blog_detail.html"
