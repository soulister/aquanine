# encoding=utf8
import json
import urllib

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, QueryDict, JsonResponse
from django.views import generic
from shop.models import *


class CartView(generic.View):
    def get_object(self):
        self.request.session.set_expiry(0)
        cart_id = self.request.session.get("cart_id")
        if cart_id is None:
            cart = Cart()
            cart.save()
            cart_id = cart.id
            self.request.session["cart_id"] = cart_id

        cart = Cart.objects.get(id=cart_id)
        if self.request.user.is_authenticated():
            cart.member = self.request.user.member
            cart.save()

        return cart

    def get(self, request):
        context = {}
        request.session.set_expiry(0)  # 5 minutes
        cart_id = request.session.get("cart_id")
        context['items'] = CartItem.objects.filter(cart=cart_id)
        return render(request, 'shop/cart.html', context)

    def put(self, request):
        request.session.set_expiry(0)  # 5 minutes
        cart_id = request.session.get("cart_id")

        request_body = QueryDict(urllib.unquote(request.body))
        data_dict = json.loads(request_body['data'])
        carts = []
        for item in data_dict:
            cart_item = CartItem.objects.get(pk=item['id'])
            if item['qty'] == 0:
                cart_item.delete()
            else:
                cart_item.quantity = item['qty']
                cart_item.save()
                carts.append(cart_item.quantity)

        cart = Cart.objects.get(pk=cart_id)
        cart.total = request_body['total']
        cart.tax_delivery = request_body['tax_delivery']
        cart.product_total = request_body['product_total']
        cart.save()

        request.session['deliver_by'] = request_body['deliver_by']

        messages.add_message(request, messages.SUCCESS, "Cart has been updated successfully.")

        return JsonResponse({})

    def post(self, request):
        cart = self.get_object()
        items = request.POST.getlist('items')
        message = ""
        for item in items:
            item_instance = get_object_or_404(Stock, id=item)
            qty = request.GET.get("qty", 1)

            cart_item, created = CartItem.objects.get_or_create(cart=cart, item=item_instance)
            if created:
                message = "Successfully added to the cart"
            else:
                message = "Quantity has been updated successfully."
                cart_item.quantity += qty
                cart_item.save()

            cart.total += cart_item.item.product.price
            cart.save()

        if request.is_ajax():
            try:
                total = cart.total
            except:
                total = 0

            try:
                total_items = cart.items.count()
            except:
                total_items = 0

            data = {
                "total": total,
                "message": message,
                "total_items": total_items
            }

            return JsonResponse(data)

        messages.add_message(request, messages.SUCCESS, message)

        return HttpResponseRedirect(reverse("shop:cart"))

    def delete(self, request):
        request.session.set_expiry(0)  # 5 minutes
        cart_id = request.session.get("cart_id")
        request_body = QueryDict(urllib.unquote(request.body))

        item_id = request_body['id']
        cart_item = CartItem.objects.get(pk=item_id)
        cart_item.delete()

        cart = Cart.objects.get(pk=cart_id)
        items = CartItem.objects.filter(cart=cart_id)
        total = 0
        for item in items:
            total += (item.quantity * item.item.product.price)

        cart.total = format(total, '.2f')
        cart.save()

        messages.add_message(request, messages.SUCCESS, "Item has been deleted successfully.")
        return JsonResponse({})
