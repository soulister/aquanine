# encoding=utf8

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.views import generic

from shop.models import *


class ProductListView(generic.View):
    def get(self, request):
        context = {}
        product_lists = Product.objects
        context['params'] = {}
        for k, val in request.GET.lists():
            if k != 'limit' and k != 'page':
                values = []
                for v in val:
                    values = values + v.split(',')
                    product_lists = product_lists.filter(**{"{0}__in".format(k): values})

                context['params'][k] = values

        product_lists = product_lists.order_by('id')

        limit = request.GET.get('limit') if 'limit' in request.GET else 9
        paginator = Paginator(product_lists, limit)

        context['limit'] = limit
        page = request.GET.get('page')
        try:
            context['products'] = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            context['products'] = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            context['products'] = paginator.page(paginator.num_pages)

        return render(request, 'shop/product_list.html', context)


class ProductDetailView(generic.View):
    def get(self, request, slug):
        context = {'product': Product.objects.get(slug=slug),
                   'products_show': Product.objects.exclude(slug=slug).order_by('?')[:9]}
        return render(request, 'shop/product_single.html', context)
