# encoding=utf8

from django.conf import settings
from django.utils.six import wraps
from django.contrib import messages
from django.core.mail import send_mail
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.db.models import Q
from django.views import generic
from shop.models import *
from shop.forms import UserForm

TODAY = date.today()


def require_ajax(view):
    @wraps(view)
    def _wrapped_view(request, *args, **kwargs):
        if request.is_ajax():
            return view(request, *args, **kwargs)
        else:
            raise PermissionDenied()

    return _wrapped_view


@require_ajax
def item_count(request):
    cart_id = request.session.get("cart_id")
    if cart_id is None:
        count = 0
    else:
        cart = Cart.objects.get(id=cart_id)
        count = cart.items.count()

    return JsonResponse({"count": count})


class IndexView(generic.View):
    def get(self, request):
        context = {'announces': Announce.objects.filter(Q(start__lte=TODAY), Q(end__gte=TODAY) | Q(end__isnull=True)),
                   'products': Product.objects.all().order_by('?')[:9]}
        return render(request, 'shop/home.html', context)


class Signup(generic.View):
    def get(self, request):
        if request.user.is_authenticated():
            messages.add_message(request, messages.ERROR, "ขออภัย! คุณเป็นสมาชิกอยู่แล้ว")
            return redirect('shop:index')
        else:
            form = UserForm()
            return render(request, 'registration/signup.html', {'form': form})

    def post(self, request):
        context = {}
        context['form'] = form = UserForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            market = form.cleaned_data['market']
            address = form.cleaned_data['address']
            province = form.cleaned_data['province']
            zipcode = form.cleaned_data['zipcode']
            is_seller = form.cleaned_data['is_seller']

            if not User.objects.filter(username=username).exists():
                if not User.objects.filter(email=email).exists():
                    user = User.objects.create_user(
                        username=username, password=password, email=email, first_name=first_name, last_name=last_name)
                    user.is_active = 1
                    user.is_staff = is_seller
                    user.save()

                    Member.objects.create(user=user, phone=phone, market=market, address=address, province=province, zipcode=zipcode)

                    if user.member.is_staff:
                        seller_name = u'ชื่อตัวแทนจำหน่าย: {0}'.format(user.get_full_name())
                        seller_address = u'สถานที่จัดจำหน่าย: {0}'.format(user.member.get_full_address())

                        subject = u'New seller is register'
                        to = ['aquanineth@gmail.com']
                        from_email = settings.EMAIL_SENDER
                        message = u'Username: {0}\n{1}\n{2}\n{3}\nดูข้อมูล: http://www.aquanineth.com/admin/member/{4}/change/'.format(
                            user.username, seller_name, user.email, seller_address, user.id)
                        send_mail(subject, message, from_email, to)

                        messages.add_message(request, messages.SUCCESS,
                                             "สมัครสมาชิกเรียบร้อยแล้ว คุณสามารถ Login เพื่อเข้าสู้ระบบ และทำการสั่งซื้อสินค้าได้แล้ว")
                    return redirect('shop:login')
                else:
                    messages.add_message(request, messages.ERROR, "Email is already used.")
                    return render(request, 'registration/signup.html', context)
            else:
                messages.add_message(request, messages.ERROR, "Username is already used.")
                return render(request, 'registration/signup.html', context)
        else:
            return render(request, 'registration/signup.html', context)
