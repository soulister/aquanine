# encoding=utf8
from django.contrib import admin

from django.contrib.auth.admin import UserAdmin
from nested_inline.admin import NestedStackedInline, NestedTabularInline, NestedModelAdmin
from shop.models import *

ADMIN_MENU_ORDER = [
    ("Users", ("auth.User", "auth.Group")),
]


# Register your models here.
class GalleryInline(NestedTabularInline):
    model = Gallery
    extra = 1
    fk_name = 'product'


class StockInline(NestedTabularInline):
    model = Stock
    extra = 1
    fk_name = 'product'


class MemberInline(NestedStackedInline):
    model = Member
    fk_name = 'user'


@admin.register(Announce)
class AnnounceAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ['name', 'start', 'end']
    search_fields = ['name']
    list_filter = ['start', 'end']


@admin.register(Product)
class ProductAdmin(NestedModelAdmin):
    ordering = ['id']
    inlines = [GalleryInline, StockInline]
    list_display = ['code', 'name', 'brand', 'color', 'price', 'discount']
    search_fields = ['name', 'code']
    list_filter = ['brand', 'color', 'price', 'discount']


class UserAdmin(UserAdmin):
    list_display = ['username', 'fullname', 'get_address', 'get_phone', 'email', 'is_staff', 'last_login']
    ordering = ['id']
    inlines = [MemberInline]
    add_fieldsets = [
        (None, {
            'classes': ['wide'],
            'fields': ['username', 'password1', 'password2', 'first_name', 'last_name', 'email']}
         ),
    ]

    def fullname(self, user):
        return user.member

    def get_phone(self, user):
        return user.member.phone

    def get_address(self, user):
        return user.member.get_full_address()

    def is_seller(self, user):
        return user.member.is_seller

    is_seller.boolean = True
    fullname.short_description = 'Name'
    get_address.short_description = 'Address'
    get_phone.short_description = 'Phone'


admin.site.register(Brand)
admin.site.register(Category)
# Re-register UserAdmin, add new one with campaign user info
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
