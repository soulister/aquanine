from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import views as auth_views

from shop import views
from shop.pages import blogs
from shop.pages import products, carts, checkouts, orders

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/$', auth_views.login, {'template_name': 'shop/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'shop:index'}, name='logout'),
    url(r'^signup/$', views.Signup.as_view(), name='signup'),
    url(r'^product/$', products.ProductListView.as_view(), name='product_list'),
    url(r'^product/(?P<slug>.+)/$', products.ProductDetailView.as_view(), name='product_detail'),
    url(r'^basket/$', csrf_exempt(carts.CartView.as_view()), name='cart'),
    url(r'^basket/checkout/$', checkouts.CheckoutView.as_view(), name='checkout'),
    url(r'^basket/confirm_order/$', checkouts.ConfirmCheckoutView.as_view(), name='checkout_confirm'),
    url(r'^basket/order/$', orders.OrderView.as_view(), name='order'),
    url(r'^item-count/$', views.item_count, name='item_count'),
    url(r'^payment/bank/$', orders.PaymentView.as_view(), name='payment'),
    url(r'^order/(?P<pk>[0-9]+)/confirm/$', orders.confirm_payment, name='order_confirm'),
    url(r'^order/(?P<pk>[0-9]+)/$', orders.order_detail, name='order_detail'),
    url(r'^basket/confirm_order/update/address/$', checkouts.edit_adress, name='update_address'),
    url(r'^basket/confirm_order/update/delivery/$', checkouts.edit_delivery, name='update_delivery'),
    url(r'^articles/$', blogs.BlogListView.as_view(), name='article_list'),
    url(r'^articles/(?P<slug>.+)/$', blogs.BlogDetailView.as_view(), name='article_detail'),
]