var defaults = {
    containerID: 'toTop', // fading element id
    containerHoverID: 'toTopHover', // fading element hover id
    scrollSpeed: 1200,
    easingType: 'linear'
};

$(document).ready(function () {
    $(".megamenu").megamenu();

    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1200);
    });

    $().UItoTop({easingType: 'easeOutQuart'});
});

function notify(message, type) {
    if (type === 'warning') {
        alertify.warning(message);
    } else if (type === 'error') {
        alertify.error(message);
    } else if (type === 'success') {
        alertify.success(message);
    } else {
        alertify.message(message);
    }
}