# encoding=utf8
import datetime
from django import forms
from django.core.validators import RegexValidator

TODAY = datetime.date.today()
YESTERDAY = (TODAY - datetime.timedelta(1))
LAST2DAY = (TODAY - datetime.timedelta(2))

month_lists = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']

provinces_choice = [
    ("กรุงเทพมหานคร", "กรุงเทพมหานคร"), ("กระบี่", "กระบี่"), ("กาญจนบุรี", "กาญจนบุรี"), ("กาฬสินธุ์", "กาฬสินธุ์"),
    ("กำแพงเพชร", "กำแพงเพชร"), ("ขอนแก่น", "ขอนแก่น"), ("จันทบุรี", "ขอนแก่น"), ("ฉะเชิงเทรา", "ฉะเชิงเทรา"),
    ("ชลบุรี", "ชลบุรี"), ("ชัยนาท", "ชัยนาท"), ("ชัยภูมิ", "ชัยภูมิ"), ("ชุมพร", "ชุมพร"), ("เชียงราย", "เชียงราย"),
    ("เชียงใหม่", "เชียงใหม่"), ("ตรัง", "ตรัง"), ("ตราด", "ตราด"), ("ตาก", "ตาก"), ("นครนายก", "นครนายก"),
    ("นครปฐม", "นครปฐม"), ("นครพนม", "นครพนม"), ("นครราชสีมา", "นครราชสีมา"), ("นครศรีธรรมราช", "นครศรีธรรมราช"),
    ("นครสวรรค์", "นครสวรรค์"), ("นนทบุรี", "นนทบุรี"), ("นราธิวาส", "นราธิวาส"), ("น่าน", "น่าน"),
    ("บุรีรัมย์", "บุรีรัมย์"), ("บึงกาฬ", "บึงกาฬ"), ("ปทุมธานี", "ปทุมธานี"), ("ประจวบคีรีขันธ์", "ประจวบคีรีขันธ์"),
    ("ปราจีนบุรี", "ปราจีนบุรี"), ("ปัตตานี", "ปัตตานี"), ("พระนครศรีอยุธยา", "พระนครศรีอยุธยา"), ("พะเยา", "พะเยา"),
    ("พังงา", "พังงา"), ("พัทลุง", "พัทลุง"), ("พิจิตร", "พิจิตร"), ("พิษณุโลก", "พิษณุโลก"), ("เพชรบุรี", "เพชรบุรี"),
    ("เพชรบูรณ์", "เพชรบูรณ์"), ("แพร่", "แพร่"), ("ภูเก็ต", "ภูเก็ต"), ("มหาสารคาม", "มหาสารคาม"),
    ("มุกดาหาร", "มุกดาหาร"), ("แม่ฮ่องสอน", "แม่ฮ่องสอน"), ("ยโสธร", "ยโสธร"), ("ยะลา", "ยะลา"),
    ("ร้อยเอ็ด", "ร้อยเอ็ด"),
    ("ระนอง", "ระนอง"), ("ระยอง", "ระยอง"), ("ราชบุรี", "ราชบุรี"), ("ลพบุรี", "ลพบุรี"), ("ลำปาง", "ลำปาง"),
    ("ลำพูน", "ลำพูน"), ("เลย", "เลย"), ("ศรีสะเกษ", "ศรีสะเกษ"), ("สกลนคร", "สกลนคร"), ("สงขลา", "สงขลา"),
    ("สตูล", "สตูล"), ("สมุทรปราการ", "สมุทรปราการ"), ("สมุทรสงคราม", "สมุทรสงคราม"), ("สมุทรสาคร", "สมุทรสาคร"),
    ("สระแก้ว", "สระแก้ว"), ("สระบุรี", "สระบุรี"), ("สิงห์บุรี", "สิงห์บุรี"), ("สุโขทัย", "สุโขทัย"),
    ("สุพรรณบุรี", "สุพรรณบุรี"), ("สุราษฎร์ธานี", "สุราษฎร์ธานี"), ("สุรินทร์", "สุรินทร์"), ("หนองคาย", "หนองคาย"),
    ("หนองบัวลำภู", "หนองบัวลำภู"), ("อ่างทอง", "อ่างทอง"), ("อำนาจเจริญ", "อำนาจเจริญ"), ("อุดรธานี", "อุดรธานี"),
    ("อุตรดิตถ์", "อุตรดิตถ์"), ("อุทัยธานี", "อุทัยธานี"), ("อุบลราชธานี", "อุบลราชธานี")]

bank_choice = [('', 'กรุณาเลือกธนาคาร'), ('scb', 'ธนาคารไทยพาณิชย์'), ('kbank', 'ธนาคารกสิกรไทย')]
last2day_choice = [('', 'กรุณาเลือกวันที่'), (TODAY, '{0} {1}'.format(TODAY.day, month_lists[TODAY.month-1])), (YESTERDAY, '{0} {1}'.format(YESTERDAY.day, month_lists[YESTERDAY.month-1])), (LAST2DAY, '{0} {1}'.format(LAST2DAY.day, month_lists[LAST2DAY.month-1]))]


class UserForm(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    username = forms.CharField(max_length=100, min_length=5, validators=[RegexValidator(r'^[a-z0-9]+$')],
                               error_messages={'min_length': 'กรุณากรอก Username ขั้นต่ำ 5 ตัวอักษร',
                                               'invalid': 'Username กรอกได้แค่ a-z และ 0-9 เท่านั้น'})
    email = forms.EmailField(error_messages={'invalid': 'กรุณากรอก Email ให้ถูกต้อง (Ex. example@email.com)'})
    password = forms.CharField(max_length=100, min_length=6, widget=forms.PasswordInput,
                               error_messages={'min_length': 'กรุณากรอก Username ขั้นต่ำ 6 ตัวอักษร'})
    confirm_password = forms.CharField(max_length=100, min_length=6, widget=forms.PasswordInput,
                                       error_messages={'min_length': 'กรุณากรอก Username ขั้นต่ำ 6 ตัวอักษร'})
    phone = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs={'pattern': '\d{9,10}', 'title': 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง ไม่ต้องมี - คั่น และกรอกได้เฉพาะตัวเลขเท่านั้น'}), validators=[RegexValidator(r'^\d{9,10}$')], error_messages={'invalid': 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง ไม่ต้องมี - คั่น และกรอกได้เฉพาะตัวเลขเท่านั้น'})
    market = forms.CharField(max_length=255, required=False)
    address = forms.CharField(widget=forms.Textarea)
    province = forms.ChoiceField(choices=provinces_choice)
    zipcode = forms.CharField(max_length=5, min_length=5, widget=forms.TextInput(attrs={'pattern': '\d{5}', 'title': 'รหัสไปรษณีย์ควรกรอกให้ครบ 5 ตัวอักษร'}), validators=[RegexValidator(r'^\d{5}$')],
                              error_messages={'min_length': 'รหัสไปรษณีย์ควรกรอกให้ครบ 5 ตัวอักษร',
                                              'invalid': 'กรุณากรอกตัวเลขเท่านั้น'})
    is_seller = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = self.cleaned_data  # individual field's clean methods have already been called
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError("รหัสผ่านไม่ตรงกัน")

        return cleaned_data


class AddressForm(forms.Form):
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    email = forms.EmailField(error_messages={'invalid': 'กรุณากรอก Email ให้ถูกต้อง (Ex. example@email.com)'})
    phone = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs={'pattern': '\d{9,10}', 'title': 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง ไม่ต้องมี - คั่น และกรอกได้เฉพาะตัวเลขเท่านั้น'}), validators=[RegexValidator(r'^\d{9,10}$')], error_messages={'invalid': 'กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง ไม่ต้องมี - คั่น และกรอกได้เฉพาะตัวเลขเท่านั้น'})
    address = forms.CharField(widget=forms.Textarea)
    province = forms.ChoiceField(choices=provinces_choice)
    zipcode = forms.CharField(max_length=5, min_length=5, widget=forms.TextInput(attrs={'pattern': '\d{5}', 'title': 'รหัสไปรษณีย์ควรกรอกให้ครบ 5 ตัวอักษร'}), validators=[RegexValidator(r'^\d{5}$')],
                              error_messages={'min_length': 'รหัสไปรษณีย์ควรกรอกให้ครบ 5 ตัวอักษร',
                                              'invalid': 'กรุณากรอกตัวเลขเท่านั้น'})


class BankPaymentForm(forms.Form):
    order = forms.CharField(max_length=6, widget=forms.TextInput(attrs={'pattern': '\d+', 'title': 'กรุณากรอกเลขที่ออเดอร์'}), validators=[RegexValidator(r'^\d+$')], error_messages={'invalid': 'กรุณากรอกเลขที่ออเดอร์'})
    bank = forms.ChoiceField(choices=bank_choice)
    on_transfer = forms.ChoiceField(choices=last2day_choice)
    on_time = forms.CharField(max_length=5, widget=forms.TextInput(attrs={'pattern': '[0-2]\d.[0-6]\d', 'title': 'กรุณากรอกเวลาให้ถูกต้อง (ตัวอย่าง 13.00)'}), validators=[RegexValidator(r'^[0-2]\d.[0-6]\d$')], error_messages={'invalid': 'กรุณากรอกเวลาให้ถูกต้อง (ตัวอย่าง 13.00)'})
    money = forms.CharField(max_length=12, widget=forms.TextInput(attrs={'pattern': '\d+\.?\d?\d?', 'title': 'กรุณากรอกจำนวนเงินให้ถูกต้อง (ตัวอย่าง 1300.07)'}), validators=[RegexValidator(r'^\d+\.?\d?\d?$')], error_messages={'invalid': 'กรุณากรอกจำนวนเงินให้ถูกต้อง (ตัวอย่าง 1300.07)'})
    slip_transfer = forms.FileField(required=False)