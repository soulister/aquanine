# encoding=utf8
from django.core.management.base import BaseCommand

from shop.models import Member


class Command(BaseCommand):

    def handle(self, *args, **options):
        members = Member.objects.filter(is_seller=True)
        for member in members:
            member.user.is_staff = True
            member.user.save()