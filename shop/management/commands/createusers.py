# encoding=utf8
from shop.models import Member
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        users = []
        if not User.objects.filter(username="admin").exists():
            user = User.objects.create_user(
                username="admin", password="Soul2536", email="admin@impic.co")
            user.is_superuser = True
            user.is_staff = True
            user.save()
            users.append(user)
        if not User.objects.filter(username="aquanine").exists():
            user = User.objects.create_user(
                username="aquanine", password="aq123456789", email="aquanine@gmail.com")
            user.is_superuser = True
            user.is_staff = True
            user.save()
            users.append(user)

        for user in users:
            if not hasattr(user, 'member'):
                # superadmin should not be a campaignuser
                Member.objects.create(user=user)
