# encoding=utf8
from django.utils.text import slugify

from shop.models import Product
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        products = Product.objects.all()
        for product in products:
            if product.color == None:
                slug = '{0}-{1}'.format(product.name, product.id)
            else:
                slug = '{0}-{1}'.format(product.name, product.color.name_en)

            product.slug = slugify(slug, True)
            product.save()