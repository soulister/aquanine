# encoding=utf8
from shop.models import Product, Stock
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        products = Product.objects.all()
        for product in products:
            if Stock.objects.filter(product=product.id, size='s').count() == 0:
                Stock.objects.create(product=product, size='s')

            if Stock.objects.filter(product=product.id, size='m').count() == 0:
                Stock.objects.create(product=product, size='m')

            if Stock.objects.filter(product=product.id, size='l').count() == 0:
                Stock.objects.create(product=product, size='l')

            if Stock.objects.filter(product=product.id, size='xl').count() == 0:
                Stock.objects.create(product=product, size='xl')