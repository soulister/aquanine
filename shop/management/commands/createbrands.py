# encoding=utf8
from shop.models import Brand
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        brands = ['Triangel', 'Aquanine', 'USA', 'NYC Racing', 'AQUA09', 'SUPER STAR', 'LONDON SOLDIER', 'LONDON ST', 'BESIDE', 'WARRIOR', 'JUMBO', 'REGULAR', 'COMET', 'NY', 'A-Star']
        for brand in brands:
            if not Brand.objects.filter(name=brand).exists():
                Brand.objects.create(name=brand)