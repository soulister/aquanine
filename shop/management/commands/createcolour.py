# encoding=utf8
from django.core.management.base import BaseCommand

from shop.models import Colour


class Command(BaseCommand):

    def handle(self, *args, **options):
        colours_list = [{'en_name': 'White', 'th_name': u'ขาว'}, {'en_name': 'Black', 'th_name': u'ดำ'}, {'en_name': 'Red', 'th_name': u'แดง'}, {'en_name': 'Blue', 'th_name': u'น้ำเงิน'}, {'en_name': 'Navy Blue', 'th_name': u'กรม'}, {'en_name': 'Top Black', 'th_name': u'ท้อปดำ'}, {'en_name': 'Top Gray', 'th_name': u'ท้อปเทา'}, {'en_name': 'Black/Black', 'th_name': u'ดำ/ดำ'}, {'en_name': 'Black/Red', 'th_name': u'ดำ/แดง'}]
        for color in colours_list:
            if Colour.objects.filter(name_th=color['th_name'], name_en=color['en_name']).count() == 0:
                Colour.objects.create(name_th=color['th_name'], name_en=color['en_name'])