# encoding=utf8

from django import template

register = template.Library()


@register.filter
def klass(ob):
    return ob.__class__.__name__