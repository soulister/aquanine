# encoding=utf8

from django import template
from shop.models import *

register = template.Library()


@register.filter
def discount_percent(obj):
    # you would need to do any localization of the result here
    return obj.discount_percent()


@register.filter
def deliver_to_name(deliver):
    # you would need to do any localization of the result here
    if deliver == 'normal':
        return u'ลงทะเบียน'

    return deliver.upper()


@register.filter
def item_total_price(qty, price):
    return qty * price


@register.inclusion_tag('tags/categories-lists.html')
def footer_menu_categories():
    categories = Category.objects.all()
    return {'categories': categories}


@register.inclusion_tag('tags/filter-menu.html')
def filter_menu(params):
    context = {}
    context['params'] = params
    context['categories'] = Category.objects.all()
    context['colour'] = Colour.objects.all()
    context['sizes'] = Stock.objects.all().values('size').distinct()
    return context