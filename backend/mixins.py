from django.contrib.auth.decorators import login_required, user_passes_test

LOGIN_URL = '/admin/login/'


def only_superadmin():
    return user_passes_test(lambda u: not u.is_anonymous() and u.is_superuser, login_url=LOGIN_URL)


class SuperAdminRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(SuperAdminRequiredMixin, cls).as_view(**initkwargs)
        superadmin_role = only_superadmin()
        return login_required(superadmin_role(view), login_url=LOGIN_URL)
