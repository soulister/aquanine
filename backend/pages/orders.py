# encoding=utf8

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from backend.mixins import SuperAdminRequiredMixin
from backend.tables import ProductTable, OrderTable, PaymentTable
from backend.forms import ProductModelForm, StockFormSet, GalleryFormSet
from shop.models import Product, Stock, Gallery, Order, CartItem, Payment


class OrderListView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        orders = OrderTable()
        return render(request, "backend/orders/lists.html", {'orders': orders})


class OrderDetailView(SuperAdminRequiredMixin, generic.DetailView):
    model = Order
    template_name = 'backend/orders/detail.html'

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data()
        order = self.get_object()

        context['items'] = CartItem.objects.filter(cart=order.cart.id)
        return context


class OrderPaymentView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        payments = PaymentTable()
        return render(request, "backend/orders/payment.html", {'payments': payments})


def confirm_payment(request, pk, status):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('backend:payment_list'))

    payment = Payment.objects.get(pk=pk)
    payment.order.is_confirm = status
    payment.order.save()

    payment.delete()
    return HttpResponseRedirect(reverse('backend:payment_list'))