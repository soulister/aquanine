# encoding=utf8
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from backend.forms import AnnounceModelForm
from backend.mixins import SuperAdminRequiredMixin
from shop.models import Announce


def delete_announce(request, pk):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('backend:announce_detail', args=[pk]))

    Announce.objects.get(pk=pk).delete()
    return HttpResponseRedirect(reverse('backend:announce_list'))


class AnnounceListView(SuperAdminRequiredMixin, generic.ListView):
    ordering = ['-id']
    model = Announce
    template_name = "backend/announces/lists.html"


class AnnounceChangeView(SuperAdminRequiredMixin, generic.View):
    def get(self, request, pk):
        context = {}
        context['announce'] = announce = Announce.objects.get(pk=pk)
        context['form'] = AnnounceModelForm(instance=announce)
        return render(request, 'backend/announces/change.html', context)

    def post(self, request, pk):
        context = {}
        context['announce'] = announce = Announce.objects.get(pk=pk)
        context['form'] = announce_form = AnnounceModelForm(request.POST, request.FILES, instance=announce)
        if announce_form.is_valid():
            announce_form.save()
            return HttpResponseRedirect(reverse('backend:announce_list'))

        return render(request, 'backend/announces/change.html', context)


class AnnounceAddView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        context = {}
        context['form'] = AnnounceModelForm()
        return render(request, 'backend/announces/new.html', context)

    def post(self, request):
        context = {}
        context['form'] = announce_form = AnnounceModelForm(request.POST, request.FILES)
        if announce_form.is_valid():
            announce_form.save()
            return HttpResponseRedirect(reverse('backend:announce_list'))

        return render(request, 'backend/announces/new.html', context)
