# encoding=utf8
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from backend.forms import ArticleModelFormChange, ArticleModelFormAdd
from backend.mixins import SuperAdminRequiredMixin
from backend.tables import ArticleTable
from shop.models import Article


def delete_article(request, pk):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('backend:article_detail', args=[pk]))

    Article.objects.get(pk=pk).delete()
    return HttpResponseRedirect(reverse('backend:article_list'))


class ArticleListView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        articles = ArticleTable()
        return render(request, "backend/articles/lists.html", {'articles': articles})


class ArticleChangeView(SuperAdminRequiredMixin, generic.View):
    def get(self, request, pk):
        context = {}
        context['article'] = article = Article.objects.get(pk=pk)
        context['form'] = ArticleModelFormChange(instance=article)
        return render(request, 'backend/articles/change.html', context)

    def post(self, request, pk):
        context = {}
        context['product'] = product = Article.objects.get(pk=pk)
        context['form'] = product_form = ArticleModelFormChange(request.POST, request.FILES, instance=product)
        if product_form.is_valid():
            product_form.save()
            return HttpResponseRedirect(reverse('backend:article_list'))

        return render(request, 'backend/articles/change.html', context)


class ArticleAddView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        context = {}
        context['form'] = ArticleModelFormAdd()
        return render(request, 'backend/articles/new.html', context)

    def post(self, request):
        context = {}
        context['form'] = product_form = ArticleModelFormAdd(request.POST, request.FILES)
        if product_form.is_valid():
            created_product = product_form.save()
            return HttpResponseRedirect(reverse('backend:article_list'))

        return render(request, 'backend/articles/new.html', context)