# encoding=utf8
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from backend.forms import MemberCreationForm, MemberChangeForm, MemberFormSet
from backend.mixins import SuperAdminRequiredMixin
from backend.tables import MemberTable


def delete_user(request, pk):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('backend:announce_detail', args=[pk]))

    User.objects.get(pk=pk).delete()
    return HttpResponseRedirect(reverse('backend:member_list'))



class UserListView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        members = MemberTable()
        return render(request, "backend/members/lists.html", {'members': members})


class UserDetailView(SuperAdminRequiredMixin, generic.DetailView):
    model = User
    template_name = 'backend/members/detail.html'


class UserChangeView(SuperAdminRequiredMixin, generic.View):
    def get(self, request, pk):
        context = {}
        context['user'] = user = User.objects.get(pk=pk)
        context['form'] = MemberChangeForm(instance=user)
        context['formset'] = MemberFormSet(instance=user)
        return render(request, 'backend/members/change.html', context)

    def post(self, request, pk):
        context = {}
        context['user'] = user = User.objects.get(pk=pk)
        context['form'] = user_form = MemberChangeForm(request.POST, request.FILES, instance=user)
        if user_form.is_valid():
            created_user = user_form.save(commit=False)
            member_formset = MemberFormSet(request.POST, instance=created_user)
            if member_formset.is_valid():
                created_user.save()
                member_formset.save()
                return HttpResponseRedirect(reverse('backend:member_list'))

        context['formset'] = MemberFormSet(request.POST, instance=user)
        return render(request, 'backend/members/change.html', context)


class UserAddView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        context = {}
        user = User()
        context['form'] = MemberCreationForm()
        context['formset'] = MemberFormSet(instance=user)
        return render(request, 'backend/members/new.html', context)

    def post(self, request):
        context = {}
        context['form'] = user_form = MemberCreationForm(request.POST, request.FILES)
        if user_form.is_valid():
            created_user = user_form.save(commit=False)
            member_formset = MemberFormSet(request.POST, instance=created_user)
            if member_formset.is_valid():
                created_user.save()
                member_formset.save()
                return HttpResponseRedirect(reverse('backend:user_list'))

        user = User()
        context['formset'] = MemberFormSet(request.POST, instance=user)
        return render(request, 'backend/members/new.html', context)
