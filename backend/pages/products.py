# encoding=utf8

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from backend.mixins import SuperAdminRequiredMixin
from backend.tables import ProductTable
from backend.forms import ProductModelForm, StockFormSet, GalleryFormSet
from shop.models import Product, Stock, Gallery


def delete_product(request, pk):
    if not request.user.is_authenticated() or not request.user.is_superuser:
        return HttpResponseRedirect(reverse('backend:product_detail', args=[pk]))

    product = Product.objects.get(pk=pk)
    Gallery.objects.filter(product=product).delete()
    Stock.objects.filter(product=product).delete()
    product.delete()
    return HttpResponseRedirect(reverse('backend:product_list'))


class ProductListView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        products = ProductTable()
        return render(request, "backend/products/lists.html", {'products': products})


class ProductDetailView(SuperAdminRequiredMixin, generic.DetailView):
    model = Product
    template_name = 'backend/products/detail.html'


class ProductChangeView(SuperAdminRequiredMixin, generic.View):
    def get(self, request, pk):
        context = {}
        context['product'] = product = Product.objects.get(pk=pk)
        context['form'] = ProductModelForm(instance=product)
        context['stock_formset'] = StockFormSet(instance=product)
        context['gallery_formset'] = GalleryFormSet(instance=product)
        return render(request, 'backend/products/change.html', context)

    def post(self, request, pk):
        context = {}
        context['product'] = product = Product.objects.get(pk=pk)
        context['form'] = product_form = ProductModelForm(request.POST, request.FILES, instance=product)
        if product_form.is_valid():
            created_product = product_form.save(commit=False)
            stock_formset = StockFormSet(request.POST, instance=created_product)
            gallery_formset = GalleryFormSet(request.POST, request.FILES, instance=created_product)
            if stock_formset.is_valid() and gallery_formset.is_valid():
                created_product.save()
                stock_formset.save()
                gallery_formset.save()
                return HttpResponseRedirect(reverse('backend:product_detail', args=[product.id]))

        context['stock_formset'] = StockFormSet(request.POST, instance=product)
        context['gallery_formset'] = GalleryFormSet(request.POST, request.FILES, instance=product)
        return render(request, 'backend/products/change.html', context)


class ProductAddView(SuperAdminRequiredMixin, generic.View):
    def get(self, request):
        context = {}
        product = Product()

        context['form'] = ProductModelForm()
        context['stock_formset'] = StockFormSet(instance=product)
        context['gallery_formset'] = GalleryFormSet(instance=product)
        return render(request, 'backend/products/new.html', context)

    def post(self, request):
        context = {}
        context['form'] = product_form = ProductModelForm(request.POST, request.FILES)
        if product_form.is_valid():
            created_product = product_form.save(commit=False)
            stock_formset = StockFormSet(request.POST, instance=created_product)
            gallery_formset = GalleryFormSet(request.POST, request.FILES, instance=created_product)
            if stock_formset.is_valid() and gallery_formset.is_valid():
                created_product.save()
                stock_formset.save()
                gallery_formset.save()
                return HttpResponseRedirect(reverse('backend:product_detail', args=[created_product.id]))

        product = Product()
        context['formset'] = StockFormSet(request.POST, instance=product)
        context['gallery_formset'] = GalleryFormSet(request.POST, request.FILES, instance=product)

        return render(request, 'backend/products/new.html', context)