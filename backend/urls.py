from django.conf.urls import url
from django.contrib.auth import views as auth_views

from backend import views
from backend.pages import announces
from backend.pages import blogs
from backend.pages import members
from backend.pages import orders

from backend.pages import products

urlpatterns = [
    # url('^',include('django.contrib.auth.urls')),
    url(r'^$', views.DashboardView.as_view(), name='dashboard'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'backend:login'}, name='logout'),
    url(r'^product/$', products.ProductListView.as_view(), name='product_list'),
    url(r'^product/new/$', products.ProductAddView.as_view(), name='product_add'),
    url(r'^product/(?P<pk>[0-9]+)/$', products.ProductDetailView.as_view(), name='product_detail'),
    url(r'^product/(?P<pk>[0-9]+)/change/$', products.ProductChangeView.as_view(), name='product_change'),
    url(r'^product/(?P<pk>[0-9]+)/delete/$', products.delete_product, name='product_delete'),

    url(r'^announce/$', announces.AnnounceListView.as_view(), name='announce_list'),
    url(r'^announce/new/$', announces.AnnounceAddView.as_view(), name='announce_add'),
    url(r'^announce/(?P<pk>[0-9]+)/change/$', announces.AnnounceChangeView.as_view(), name='announce_change'),
    url(r'^announce/(?P<pk>[0-9]+)/delete/$', announces.delete_announce, name='announce_delete'),

    url(r'^member/$', members.UserListView.as_view(), name='member_list'),
    url(r'^member/new/$', members.UserAddView.as_view(), name='member_add'),
    # url(r'^member/(?P<pk>[0-9]+)/$', members.UserDetailView.as_view(), name='member_detail'),
    url(r'^member/(?P<pk>[0-9]+)/change/$', members.UserChangeView.as_view(), name='member_change'),
    url(r'^member/(?P<pk>[0-9]+)/delete/$', members.delete_user, name='member_delete'),

    url(r'^order/$', orders.OrderListView.as_view(), name='order_list'),
    url(r'^order/payment/$', orders.OrderPaymentView.as_view(), name='payment_list'),
    url(r'^order/payment/(?P<pk>[0-9]+)/(?P<status>[01])/$', orders.confirm_payment, name='payment_update'),
    url(r'^order/(?P<pk>[0-9]+)/$', orders.OrderDetailView.as_view(), name='order_detail'),

    url(r'^article/$', blogs.ArticleListView.as_view(), name='article_list'),
    url(r'^article/new/$', blogs.ArticleAddView.as_view(), name='article_add'),
    url(r'^article/(?P<pk>[0-9]+)/change/$', blogs.ArticleChangeView.as_view(), name='article_change'),
    url(r'^article/(?P<pk>[0-9]+)/delete/$', blogs.delete_article, name='article_delete'),
]
