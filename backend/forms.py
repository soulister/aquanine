# encoding=utf8
from __future__ import print_function
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from shop.models import Product, Stock, Gallery, Announce, Member, Article


def change_field_to_bootstrap(fields):
    for field in iter(fields):
        if not isinstance(fields[field].widget, forms.CheckboxInput):
            fields[field].widget.attrs.update({
                'class': 'form-control'
            })

    return fields


class AnnounceModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AnnounceModelForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

        self.fields['start'].widget.attrs.update({
            'class': 'form-control date'
        })
        self.fields['end'].widget.attrs.update({
            'class': 'form-control date'
        })

    class Meta:
        model = Announce
        fields = ['name', 'start', 'end', 'image']


class MemberCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(MemberCreationForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'is_staff', 'is_superuser']


class MemberChangeForm(UserChangeForm):
    password = ReadOnlyPasswordHashField(label=("Password"), help_text=("Raw passwords are not stored, so there is no way to see this user's password, but you can change the password using <a class=\"text-primary\" href=\"../password/\">this form</a>."))

    def __init__(self, *args, **kwargs):
        super(MemberChangeForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'password', 'email', 'is_staff', 'is_superuser', 'is_active']


class ProductModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductModelForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

    class Meta:
        model = Product
        fields = ['name', 'code', 'brand', 'category', 'color', 'price', 'discount', 'detail', 'image']


class ItemForm(forms.ModelForm):
    class Meta:
        model = Stock
        fields = ['size', 'total']

    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)


class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['phone', 'market', 'address', 'province', 'zipcode']

    def __init__(self, *args, **kwargs):
        super(MemberForm, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)


class ArticleModelFormAdd(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArticleModelFormAdd, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

    class Meta:
        model = Article
        fields = ['title', 'detail', 'detail', 'keyword_tags', 'main_image', 'is_active']


class ArticleModelFormChange(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArticleModelFormChange, self).__init__(*args, **kwargs)
        change_field_to_bootstrap(self.fields)

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['slug'].widget.attrs['readonly'] = True

    class Meta:
        model = Article
        fields = ['title', 'slug', 'detail', 'detail', 'keyword_tags', 'main_image', 'is_active']


GalleryFormSet = forms.inlineformset_factory(Product, Gallery, fields=['image'], extra=1)
StockFormSet = forms.inlineformset_factory(Product, Stock, fields=['size', 'total'], extra=1, form=ItemForm)
MemberFormSet = forms.inlineformset_factory(User, Member, fields=['phone', 'market', 'address', 'province', 'zipcode'], extra=1, form=MemberForm)