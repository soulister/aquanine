# encoding=utf8
from django.contrib.auth.models import User
from django.utils.html import escape
from django.utils.safestring import mark_safe
from table import Table
from table.columns import Column
from table.columns import Link
from table.columns import LinkColumn
from table.utils import A, Accessor

from shop.models import Product, Order, Payment, Article


class OrderLink(Link):
    def render(self, obj):
        """ Render link as HTML output tag <a>.
        """
        self.obj = obj
        attrs = ' '.join([
            '%s="%s"' % (attr_name, attr.resolve(obj))
            if isinstance(attr, Accessor)
            else '%s="%s"' % (attr_name, attr)
            for attr_name, attr in self.attrs.items()
        ])
        return mark_safe(u'<a %s>%05d</a>' % (attrs, int(self.text)))


class SlipLinkColumn(Column):
    def __init__(self, header=None, field=None, **kwargs):
        super(SlipLinkColumn, self).__init__(field, header, **kwargs)

    def render(self, obj):
        slip = Accessor(self.field).resolve(obj)
        return mark_safe(u'<a href="/media/{0}" data-toggle="lightbox">ดูหลักฐานการโอนเงิน</a>'.format(slip))


class BooleanColumn(Column):
    def __init__(self, header=None, field=None, **kwargs):
        super(BooleanColumn, self).__init__(field, header, **kwargs)

    def render(self, obj):
        boolean_text = Accessor(self.field).resolve(obj)
        if boolean_text:
            return mark_safe(u'<i class="fa fa-check text-success"></i>')
        else:
            return mark_safe(u'<i class="fa fa-remove text-danger"></i>')


class CustomColumn(Column):
    def __init__(self, header=None, text=None, field=None, **kwargs):
        self.text = text
        super(CustomColumn, self).__init__(field, header, **kwargs)

    def render(self, obj):
        if self.text is None:
            text = Accessor(self.field).resolve(obj)
            return mark_safe(text)
        else:
            return mark_safe(self.text)


class ProductTable(Table):
    id = Column(field='id', header=u'#')
    code = LinkColumn(header=u'รหัสสินค้า',
                      links=[Link(text=A('code'), viewname='backend:product_detail', args=(A('id'),))])
    name = Column(field='name', header=u'ชื่อสินค้า')
    brand = Column(field='brand', header=u'ยี่ห้อ')
    category = Column(field='category', header=u'หมวดหมู๋สินค้า')
    colour = Column(field='color', header=u'สี')
    price = Column(field='price', header=u'ราคา')
    discount = Column(field='discount', header=u'ลดเหลือ')
    action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
        Link(text='ดูข้อมูล', viewname='backend:product_detail', args=(A('id'),), attrs={'class': 'btn btn-info'}),
        Link(text='แก้ไขข้อมูล', viewname='backend:product_change', args=(A('id'),),
             attrs={'class': 'btn btn-warning'}),
        Link(text='ลบข้อมูล', viewname='backend:product_delete', args=(A('id'),),
             attrs={'class': 'btn btn-danger', 'onclick': u'return confirm(\'คุณต้องการลบสินค้าชิ้นนี้หรือไม่?\');'})])

    class Meta:
        model = Product


class MemberTable(Table):
    id = Column(header=u'#', field='id')
    username = LinkColumn(header=u'Username', links=[Link(text=A('username'), viewname='backend:member_change', args=(A('id'),))])
    fullname = Column(header=u'ชื่อลูกค้า', field='get_full_name')
    address = Column(header=u'ที่อยู่', field='member.get_full_address')
    phone = Column(header=u'เบอร์โทรศัพท์', field='member.phone')
    email = Column(header=u'Email', field='email')
    is_staff = BooleanColumn(header=u'ตัวแทนจำหน่าย', field=A('is_staff'), header_attrs={'class': 'text-center'}, attrs={'class': 'text-center'})
    is_superuser = BooleanColumn(header=u'ผู้ดูแลระบบ', field=A('is_superuser'), header_attrs={'class': 'text-center'}, attrs={'class': 'text-center'})
    action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
        Link(text='แก้ไขข้อมูล', viewname='backend:member_change', args=(A('id'),),
             attrs={'class': 'btn btn-warning'}),
        Link(text='ลบข้อมูล', viewname='backend:member_delete', args=(A('id'),),
             attrs={'class': 'btn btn-danger', 'onclick': u'return confirm(\'คุณต้องการลบสมาชิกนี้หรือไม่?\');'})])
    # action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
    #     Link(text='ดูข้อมูล', viewname='backend:member_detail', args=(A('id'),), attrs={'class': 'btn btn-info'}),
    #     Link(text='แก้ไขข้อมูล', viewname='backend:member_change', args=(A('id'),),
    #          attrs={'class': 'btn btn-warning'}),
    #     Link(text='ลบข้อมูล', viewname='backend:member_delete', args=(A('id'),),
    #          attrs={'class': 'btn btn-danger', 'onclick': u'return confirm(\'คุณต้องการลบสินค้าชิ้นนี้หรือไม่?\');'})])

    class Meta:
        model = User


class OrderTable(Table):
    id = LinkColumn(header=u'Order Number', links=[OrderLink(text=A('id'), viewname='backend:order_detail', args=(A('id'),))])
    member = LinkColumn(header=u'ชื่อลูกค้า', links=[Link(text=A('member.user.get_full_name'), viewname='backend:member_change', args=(A('member.user_id'),))])
    pay_by = Column(header=u'ชำระเงินทาง', field='pay_by', attrs={'class': 'text-uppercase'})
    deliver_by = Column(header=u'วิธีการจัดส่ง', field='deliver_by', attrs={'class': 'text-uppercase'})
    is_confirm = BooleanColumn(header=u'การชำระเงิน', field='is_confirm', header_attrs={'class': 'text-center'}, attrs={'class': 'text-center'})
    action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
        Link(text='ดูข้อมูล', viewname='backend:order_detail', args=(A('id'),), attrs={'class': 'btn btn-info'})])

    class Meta:
        model = Order


class PaymentTable(Table):
    order_id = LinkColumn(header=u'Order Number', links=[OrderLink(text=A('order_id'), viewname='backend:order_detail', args=(A('order_id'),))])
    bank = Column(header=u'ธนาคาร', field='bank', attrs={'class': 'text-uppercase'})
    on_transfer = Column(header=u'วันที่โอนเงิน', field='on_transfer', attrs={'class': 'text-uppercase'})
    on_time = Column(header=u'เวลาที่โอนเงิน', field='on_time', attrs={'class': 'text-uppercase'})
    money = Column(header=u'ยอดชำระเงิน', field='money', attrs={'class': 'text-uppercase'})
    total = Column(header=u'ยอดการสั่งซื้อ', field='order.cart.total', attrs={'class': 'text-uppercase'})
    slip_transfer = SlipLinkColumn(header=u'หลักฐานการโอนเงิน', field='slip_transfer', sortable=False)
    action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
        Link(text='ยืนยัน', viewname='backend:payment_update', args=(A('id'), 1),
             attrs={'class': 'btn btn-success'}),
        Link(text='ยกเลิก', viewname='backend:payment_update', args=(A('id'), 0),
             attrs={'class': 'btn btn-danger', 'onclick': u'return confirm(\'คุณต้องการยกเลิกการชำระเงินนี้หรือไม่?\');'})])

    class Meta:
        model = Payment


class ArticleTable(Table):
    title = Column(header=u'ชื่อบทความ', field='title')
    slug = Column(header=u'slug', field='slug')
    keyword_tags = Column(header=u'Keyword', field='keyword_tags')
    is_active = BooleanColumn(header=u'สถานะ', field='is_active', header_attrs={'class': 'text-center'}, attrs={'class': 'text-center'})
    action = LinkColumn(header=u'Action', sortable=False, header_attrs={'class': 'text-center', 'style': 'width:25%;'}, attrs={'class': 'text-center'}, links=[
        Link(text='แก้ไขข้อมูล', viewname='backend:article_change', args=(A('id'),),
             attrs={'class': 'btn btn-warning'}),
        Link(text='ลบข้อมูล', viewname='backend:article_delete', args=(A('id'),),
             attrs={'class': 'btn btn-danger', 'onclick': u'return confirm(\'คุณต้องการลบบทความนี้หรือไม่?\');'})])

    class Meta:
        model = Article
